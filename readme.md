#### ML-Rules Quickstart ReadMe

For more information about the modeling language ML-Rules itself, see the ML-Rules git repository at https://git.informatik.uni-rostock.de/mosi/mlrules2

#### System requirements:
- Java Runtime Environment 8 (https://www.java.com)
- Maven 3 (at least version 3.3.1)
	- Downloads: https://maven.apache.org/download.cgi
	- Installation manual: https://maven.apache.org/install.html

	
#### Start the editor:
- Download the repository [here](https://git.informatik.uni-rostock.de/mosi/mlrules2-quickstart/-/archive/master/mlrules2-quickstart-master.zip) and unzip the archive
- Windows: double click *mlrules.cmd*
- Linux: execute *mlrules*

#### Update ML-Rules:
The used ML-Rules version is defined in "pom.xml":

	<dependency>
		 <groupId>org.jamesii</groupId>
		 <artifactId>mlrules</artifactId>
		 <version>2.2.10</version>
	</dependency>
		
Before each start of the editor, it is checked whether a new version of ML-Rules is available. If so and the user agrees, the version is updated automatically.
